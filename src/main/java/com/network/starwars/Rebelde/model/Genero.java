package com.network.starwars.Rebelde.model;

public enum Genero {

    MASCULINO (1, "Masculino"),
    FEMININO (2, "Feminino"),
    ASEXUADO (3,"Asexuado"),
    HERMAFRODITA(4, "Hermafrodita");

    private Integer cod;

    private String nome;

    private Genero (Integer cod, String nome){
        this.cod = cod;
        this.nome = nome;
    }

    public int getCod(){
        return cod;
    }

    public String getNome(){
        return nome;
    }

    public static Genero toEnum(Integer cod){
        if (cod == null)
            return null;

        for (Genero x : Genero.values()){
            if (cod.equals(x.getCod())){
                return x;
            }
        }

        throw new IllegalArgumentException("Id inválido: " + cod);
    }
}
