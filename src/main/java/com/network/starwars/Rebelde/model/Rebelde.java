package com.network.starwars.Rebelde.model;

import com.network.starwars.Item.model.Item;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

@Entity
public class Rebelde implements Serializable {
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotBlank
    private String nome;

    private int idade;

    private Integer genero;

    @ElementCollection
    @CollectionTable(name = "reporte")
    private List<Rebelde> reporte = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "rebelde")
    private Localizacao localizacao;

    @OneToMany(mappedBy = "rebelde")
    private List<Item> iventario = new ArrayList<>();

    public Rebelde() {}

    public Rebelde(@NotNull @NotBlank String nome, int idade, Integer genero) {
        this.nome = nome;
        this.idade = idade;
        this.genero = genero;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getGenero() {
        return Objects.requireNonNull(Genero.toEnum(this.genero)).getNome();
    }

    public void setGenero(Genero genero) {
        this.genero = genero.getCod();
    }

    public boolean isTraidor() {
        if (this.reporte.size() > 2)
            return true;
        else return false;
    }

    public void setReport(Rebelde rebelde) {
        this.reporte.add(rebelde);
    }

    public List<Rebelde> getReporte() {
        return reporte;
    }

    public Localizacao getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    public List<Item> getIventario() {
        return iventario;
    }

    public void setIventario(List<Item> iventario) {
        this.iventario = iventario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rebelde rebelde = (Rebelde) o;
        return id.equals(rebelde.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
