package com.network.starwars.Rebelde.service;

import com.network.starwars.Rebelde.model.Localizacao;
import com.network.starwars.Rebelde.repository.ILocalizacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LocalizacaoService {

    @Autowired
    private ILocalizacaoRepository repository;

    public Localizacao create(Localizacao localizacao){
        return repository.save(localizacao);
    }

    public Localizacao update(Long id, Localizacao local){
        Optional<Localizacao> localizacao = repository.findById(id);

        if (localizacao.isPresent()){
            local.setId(id);
            local.setRebelde(localizacao.get().getRebelde());
            return repository.save(local);
        }

        return null;
    }
}
