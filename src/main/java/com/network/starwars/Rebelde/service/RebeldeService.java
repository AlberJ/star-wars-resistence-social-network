package com.network.starwars.Rebelde.service;

import com.network.starwars.Item.model.Item;
import com.network.starwars.Item.repostory.IItemRepository;
import com.network.starwars.Rebelde.model.Localizacao;
import com.network.starwars.Rebelde.model.Rebelde;
import com.network.starwars.Rebelde.repository.ILocalizacaoRepository;
import com.network.starwars.Rebelde.repository.IRebeldeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RebeldeService {

    @Autowired
    private IRebeldeRepository rebeldeRepository;

    @Autowired
    private ILocalizacaoRepository localizacaoRepository;

    @Autowired
    private IItemRepository itemRepository;

    public Optional<Rebelde> findOne(Long id){
        return rebeldeRepository.findById(id);
    }

    public Rebelde create(Rebelde rebelde){
        Rebelde rebeldeSalvo = rebeldeRepository.save(rebelde);

        if (rebeldeSalvo == null)
            return null;

        if (rebeldeSalvo.getLocalizacao() != null) {
            Localizacao localizacao = rebeldeSalvo.getLocalizacao();
            localizacao.setRebelde(rebeldeSalvo);
            localizacaoRepository.save(localizacao);
        }

        if (rebeldeSalvo.getIventario() != null){
            for (Item i : rebeldeSalvo.getIventario()){
                i.setRebelde(rebeldeSalvo);
                itemRepository.save(i);
            }
        }

        return rebeldeSalvo;
    }

    public List<Rebelde> getAll(){
        return rebeldeRepository.findAll();
    }

    public Rebelde update(Long id, Rebelde rebelde){
        Optional<Rebelde> optionalRebelde = rebeldeRepository.findById(id);

        if (optionalRebelde.isPresent()){
            rebelde.setId(id);
            return rebeldeRepository.save(rebelde);
        }

        return null;
    }

    public Rebelde setReporte(Long id_reportador, Long id_acusado){
        Optional<Rebelde> optReportador = rebeldeRepository.findById(id_reportador);
        if (!optReportador.isPresent()){
            return null;
        }
        Rebelde reportador = optReportador.get();

        Optional<Rebelde> optAcusado = rebeldeRepository.findById((id_acusado));
        if (!optAcusado.isPresent()){
            return null;
        }
        Rebelde acusado = optAcusado.get();

 /*
        Aqui, não resitra o reporte mas gera uma resposta como se o reporte tivesse
        sido registrado normalmente. Assim o traidor não suspeitará que foi descoberto.
 */
        if (reportador.isTraidor())
            return acusado;

        for (Rebelde rebelde : acusado.getReporte()){
            if (rebelde.equals(reportador)){
                return reportador;
            }
        }

        acusado.setReport(reportador);
        return rebeldeRepository.save(acusado);
    }

}
