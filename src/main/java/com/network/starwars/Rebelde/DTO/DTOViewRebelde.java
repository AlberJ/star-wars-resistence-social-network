package com.network.starwars.Rebelde.DTO;

import com.network.starwars.Item.model.Item;
import com.network.starwars.Rebelde.model.Rebelde;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DTOViewRebelde implements Serializable {
    private static final Long serialVersionUID = 1L;

    private Long id;
    private String nome;
    private int idade;
    private String genero;
    private boolean traidor = false;
    private List<Item> iventario = new ArrayList<>();

    public DTOViewRebelde(Rebelde rebelde) {
        this.id = rebelde.getId();
        this.nome = rebelde.getNome();
        this.idade = rebelde.getIdade();
        this.genero = rebelde.getGenero();
        this.traidor = rebelde.isTraidor();
        this.iventario = rebelde.getIventario();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public boolean isTraidor() {
        return traidor;
    }

    public void setTraidor(boolean traidor) {
        this.traidor = traidor;
    }

    public List<Item> getIventario() {
        return iventario;
    }

    public void setIventario(List<Item> iventario) {
        this.iventario = iventario;
    }
}
