package com.network.starwars.Rebelde.controller;

import com.network.starwars.Rebelde.DTO.DTOViewRebelde;
import com.network.starwars.Rebelde.model.Localizacao;
import com.network.starwars.Rebelde.model.Rebelde;
import com.network.starwars.Rebelde.service.LocalizacaoService;
import com.network.starwars.Rebelde.service.RebeldeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping(value = "/rebeldes")
public class RebeldeController {

    @Autowired
    private RebeldeService rebeldeService;

    @Autowired
    private LocalizacaoService localizacaoService;

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody Rebelde rebelde){
        Rebelde rebeldeSalvo = rebeldeService.create(rebelde);

        if(rebeldeSalvo == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(rebelde);

        return ResponseEntity.status(HttpStatus.CREATED).body(new DTOViewRebelde(rebeldeSalvo));
    }

    @PutMapping(value = "/localizacao/update/{id}")
    public ResponseEntity<Rebelde> update(@PathVariable Long id,
                                           @Valid @RequestBody Localizacao localizacao)
    {
        Localizacao local = localizacaoService.update(id, localizacao);

        if (local == null)
            return ResponseEntity.notFound().build();

        Optional<Rebelde> rebelde = rebeldeService.findOne(local.getRebelde().getId());
        if (rebelde.isPresent())
            return ResponseEntity.ok(rebelde.get());
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping
    public ResponseEntity<List<DTOViewRebelde>> getAll(){
        List<DTOViewRebelde> listaDTO = new ArrayList<>();
        for (Rebelde rebelde : rebeldeService.getAll()){
            listaDTO.add(new DTOViewRebelde(rebelde));
        }
        return ResponseEntity.ok().body(listaDTO);
    }

    @PostMapping(value = "/report")
    public ResponseEntity<?> setReport(@RequestBody Map<String, Long> json){
        Long id_reportador = json.get("id_reportador");
        Long id_acusado = json.get("id_acusado");

        if (Objects.equals(id_acusado, id_reportador)) return ResponseEntity.status(HttpStatus.CONFLICT).build();

        Rebelde rebeldeResposta = rebeldeService.setReporte(id_reportador, id_acusado);
        if (rebeldeResposta.getId().equals(id_reportador))
            return ResponseEntity.status(HttpStatus.OK).body("Reporte já registrado anteriormente.");

        return ResponseEntity.ok(new DTOViewRebelde(rebeldeResposta));
    }
}
