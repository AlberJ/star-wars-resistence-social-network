package com.network.starwars.Rebelde.repository;

import com.network.starwars.Rebelde.model.Localizacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ILocalizacaoRepository extends JpaRepository<Localizacao, Long> {}
