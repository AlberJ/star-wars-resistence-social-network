package com.network.starwars.Rebelde.repository;

import com.network.starwars.Rebelde.model.Rebelde;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRebeldeRepository extends JpaRepository<Rebelde, Long> {}
