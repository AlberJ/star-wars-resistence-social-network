package com.network.starwars.Item.service;

import com.network.starwars.Item.model.Item;
import com.network.starwars.Item.repostory.IItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemService {

    @Autowired
    private IItemRepository repository;

    public Item create(Item item){
        return repository.save(item);
    }

}
