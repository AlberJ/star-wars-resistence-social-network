package com.network.starwars;

import com.network.starwars.Item.model.Item;
import com.network.starwars.Item.repostory.IItemRepository;
import com.network.starwars.Rebelde.model.Genero;
import com.network.starwars.Rebelde.model.Localizacao;
import com.network.starwars.Rebelde.model.Rebelde;
import com.network.starwars.Rebelde.repository.ILocalizacaoRepository;
import com.network.starwars.Rebelde.repository.IRebeldeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class StarwarsApplication implements CommandLineRunner {

	@Autowired
	private IRebeldeRepository rebeldeRepository;

	@Autowired
	private ILocalizacaoRepository localizacaoRepository;

	@Autowired
	private IItemRepository itemRepository;

	public static void main(String[] args) {
		SpringApplication.run(StarwarsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
