**End Points:**


*  **Lista os Rebeldes cadastrados**

GET: [http://127.0.0.1:8080/rebeldes](http://127.0.0.1:8080/rebeldes)




*  **Cadastra Rebeldes**

Post:  [http://127.0.0.1:8080/rebeldes](http://127.0.0.1:8080/rebeldes)
BODY exemplo:
```
{
     "nome": "Tuskar",
     "idade": 55,
     "genero": 1,
     "localizacao": {
     	"latitude": 99.9,
 	"longitude": 99.9,
     	"nomeBase": "Campos frios"
     },
     "iventario":[
     	{
     		"nome": "Deadalo",
     		"pontos": 4
     	},
     	{
     		"nome": "Shadow Blade",
     		"pontos": 3
     	},
     	{
     		"nome": "Grevas Guardian",
     		"pontos": 1
 	    }
    ]
 }
```




*  **Altera localização de Rebelde**

Put:  [http://127.0.0.1:8080/rebeldes/localizacao/update/6](http://127.0.0.1:8080/rebeldes/localizacao/update/6)
Body exemplo:

```
{
	"id": 6,
	"latitude": 12.3,
	"longitude": 32.1,
	"nomeBase": "Campos gelados"
}
```




*  **Reporta traidor**

Post: [http://127.0.0.1:8080/rebeldes/report](http://127.0.0.1:8080/rebeldes/report)

Body exemplo:
```
{
    "id_reportador": 6,
    "id_acusado": 7
}
```
